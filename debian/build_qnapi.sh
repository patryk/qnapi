#! /bin/sh

# When in clone of Git repository (see Vcs-Git in debian/control), one may use
# this script for building QNapi packages

set -e
set -x

n=$(expr $(head -1 debian/changelog | awk '{printf $2}' | /usr/bin/wc -c) - 1)
version=$(head -1 debian/changelog | awk '{printf $2}' | cut -b2-$n)
upstream_version=$(echo $version | awk -F "-" '{printf $1}')
debian_revision=$(echo $version | awk -F "-" '{printf $2}')

git clean -d -f -x
git reset --hard
git co upstream
git br -D ${upstream_version}/master || true
git co -b ${upstream_version}/master upstream
git merge -m "Merging ${upstream_version}/debian" debian

debuild -us -uc -i.git $@

